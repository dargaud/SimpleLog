TARGET=SimpleLog
GCC=gcc
COPT=

all:
	$(GCC) $(COPT) -c $(TARGET).c
	$(GCC) $(COPT) -o  $(TARGET) $(TARGET).c -DSL_STANDALONE
