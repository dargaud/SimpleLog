Simple C library that offers filtering of log messages by time period or number of repeats.  
4 levels of severity.  
Displays source line of the message.  
Use:  
* Call SimpleLog_Setup() to setup to kind of reporting you want
* Optional date of message
* Output either to stdout, stderr or dedicated file
* Call SimpleLog_FilterLevel() to select which messages need to be displayed
* Call SLOG(SWRN, "Problem with input %s", SomeVar) to log a message
* Call SimpleLog_Flush() before quitting so the last messages in memory are published
* Optional: call back mechanism to get a copy of printed messages

Can also be compiled as a shell utility as a sort of 'super-uniq' with: gcc -o SimpleLog SimpleLog.c -DSL_STANDALONE
Use "SimpleLog -h" for help
